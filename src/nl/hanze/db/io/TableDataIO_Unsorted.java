package nl.hanze.db.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import nl.hanze.db.def.TableDefinition;

public class TableDataIO_Unsorted extends TableDataIO {
	
	private ArrayList<String> cache = new ArrayList<String>();
	
	public TableDataIO_Unsorted(TableDefinition def) throws Exception {
		super(def);
	}

	/**** OPGAVE 3e ***/
	@Override
	public long add(String[] record) throws Exception {
		long s = System.currentTimeMillis();

		long pkpos = pkValLocation(record[def.getColPosition(def.getPK())]);
		if (pkpos != -1)
			throw new Exception("Primary key already in table");

		FileWriter fw = new FileWriter(BaseIO.getInitDir() + File.separator
				+ def.getTableName() + ".tbl", true);
		StringBuffer temp = new StringBuffer();
		Integer[] size = def.getSizes();
		if (size.length != record.length) {
			fw.close();
			throw new Exception("Malformed record");
		}

		for (int i = 0; i < record.length; i++) {
			temp.append(appendSpaces(record[i], size[i]));
			if (i != record.length - 1) {
				temp.append("#");
			}
		}
		temp.append("\r\n");

		fw.write(temp.toString());
		fw.close();
		long e = System.currentTimeMillis();
		return e - s;
	}
	
	/*** OPGAVE 3g ***/
	@Override
	public long delete(String colname, String value) throws Exception {
		long s = System.currentTimeMillis();
		
		if (!colname.equals(def.getPK())) {
			throw new Exception("Colname is not primary key");
		}
		
		removeLine(value);

		long e = System.currentTimeMillis();
		return e - s;
	}

	/*** OPGAVE 3h ***/
	@Override
	public long update(String[] record) throws Exception {
		long s = System.currentTimeMillis();
		
		String pk = record[def.getColPosition(def.getPK())];
		
		//removeLine(pk);

		long e = System.currentTimeMillis();
		return e - s;
	}
	
	private void removeLine(String pk) {
		try {
            File inputFile = new File(BaseIO.getInitDir() + File.separator + def.getTableName() + ".tbl");
            if (!inputFile.isFile()) {
                System.out.println("Parameter is not an existing file");
                return;
            }
            //Construct the new file that will later be renamed to the original filename.
            File tempFile = new File(inputFile.getAbsolutePath() + ".tmp");
            BufferedReader br = new BufferedReader(new FileReader(BaseIO.getInitDir() + File.separator + def.getTableName() + ".tbl"));
            PrintWriter pw = new PrintWriter(new FileWriter(tempFile));
            String line = null;
 
            //Read from the original file and write to the new
            //unless content matches data to be removed.
            while ((line = br.readLine()) != null) {
            	String pkstring = line.split("#")[def.getColPosition(def.getPK())];
                if (!pkstring.equals(pk)) {
                    pw.println(line);
                    pw.flush();
                }
            }
            pw.close();
            br.close();
 
            //Delete the original file
            if (!inputFile.delete()) {
                System.out.println("Could not delete file");
                return;
            }
 
            //Rename the new file to the filename the original file had.
            if (!tempFile.renameTo(inputFile))
                System.out.println("Could not rename file");
            }
        catch (Exception e) {
        	
        }
	}

	/*** OPGAVE 3i ***/
	@Override
	public long search(String colname, String value, ArrayList<String[]> result)
			throws Exception {
		long s = System.currentTimeMillis();

		long e = System.currentTimeMillis();
		return e - s;
	}

	/*** OPGAVE 3c ***/
	/** 
	 * @param pkval
	 * @return 
	 * @throws Exception
	 */
	private long pkValLocation(String pkval) throws Exception {
		
		if (cache.contains(pkval)) {
			return cache.indexOf(pkval);
		}
		
		int col = def.getColPosition(def.getPK());
		boolean found = false;
		
		long i = 0;
		if (cache.size() > 0) {
			for (i = cache.size()-1; i < numOfRecords() && !found; i++) {
				String record = recordAt(i)[col];
				cache.add((int)i, record);
				found = record.equals(pkval);
			}
		}
		
		return found ? i - 1 : -1;

	}

}
